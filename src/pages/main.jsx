import { TableElement } from '../components/tables/table';
import { MatchTableElement } from '../components/tables/matchTable';
import { InputElements } from '../components/input/input'

export default function MainPage() {
    return (
        <div>
            <InputElements />
            <br />
            <TableElement data={null} />
            <br />
            <MatchTableElement />
        </div>
    )
}