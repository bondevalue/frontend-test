import { useEffect, useState } from "react";
import { Button, MenuItem, TextField, Paper } from "@mui/material";
import "./input.css";

export function InputElements() {
    const [sendValues, setSendValues] = useState({});

    // Input changes event handlers to build JSON
    const handleChange = (event) => {
        let value = event.target.value;
        let name = event.target.name;

        setSendValues((prevalue) => {
            return {
                ...prevalue,             
                [name]: value
            }
        })
    };

    // Input form submit event
    const handleSubmit = () => {
        !sendValues.orderType ? sendValues.orderType = 'Buy' : null

        if (sendValues.name && sendValues.orderType && sendValues.price) { 
            fetch("http://localhost:3000/orders/placement",
            {
                method: "POST",
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(sendValues)
            })
            .then(response => response.json())
            .then(data => setTableData(data.data))
            .catch(err => console.error(err))
            // TODO: Improve this way to reload the page
            window.location.reload()
        } else {
            alert('Some information is missing')
        }
    };

    return (
        <Paper>
            <div className="input_form">
                <TextField name="name" variant="filled" label="Name" onChange={handleChange} />
                <TextField style={{"width": "150px"}} name="orderType" variant="filled" label="orderType" select defaultValue="Buy" onChange={handleChange}>
                    <MenuItem key={1} value="Buy">Buy</MenuItem>
                    <MenuItem key={2} value="Sell">Sell</MenuItem>
                </TextField>
                <TextField name="price" variant="filled" label="Price" type="number" onChange={handleChange} />
                <Button variant="filled" color="primary" onClick={handleSubmit}>Place Order</Button>
            </div>
        </Paper>
    )
}