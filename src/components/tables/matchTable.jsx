import { useState } from 'react'
import { Table, TableCell, TableBody, TableRow, TableContainer, TableHead, Paper, Button, Grid } from '@mui/material';

export function MatchTableElement() {
    const [matchData, setMatchData] = useState([]);

    const handleRetrieveMatchData = (data) => {
        fetch("http://localhost:3000/orders/match")
        .then(response => response.json())
        .then(data => setMatchData(data.data))
        .catch(err => console.error(err))
    }

    return (
        <div>
            <Button style={{"backgroundColor": "darkblue"}} variant="contained" onClick={handleRetrieveMatchData} >
                Generate Match
            </Button>
            <Grid container spacing={2}>
                <Grid item xs={6}>
                <Paper>
                    <h3> Matched Orders </h3>
                    <TableContainer
                        component={Paper}
                        variant="outlined"
                    >
                        <Table aria-label="demo table">
                            <TableHead>
                                <TableRow>
                                    <TableCell>Buyer Name</TableCell>
                                    <TableCell>Seller Name</TableCell>
                                    <TableCell>Price</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {matchData && matchData.matched?.map((item) =>
                                    <TableRow key={item.buyer?.id}>
                                        <TableCell>{item.buyer?.name}</TableCell>
                                        <TableCell>{item.seller?.name}</TableCell>
                                        <TableCell>{item.buyer?.price}</TableCell>
                                    </TableRow>
                                )}
                            </TableBody>
                        </Table>
                    </TableContainer>
                    <br />
                </Paper>
                </Grid>
                <Grid item xs={6}>
                <Paper>
                    <h3> Unmatched Orders </h3>
                    <TableContainer
                        component={Paper}
                        variant="outlined"
                    >
                        <Table aria-label="demo table">
                            <TableHead>
                                <TableRow>
                                    <TableCell>Name</TableCell>
                                    <TableCell>Order Type</TableCell>
                                    <TableCell>Price</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {matchData && matchData.unmatched?.map((item) =>
                                    <TableRow key={item.id}>
                                        <TableCell>{item.name}</TableCell>
                                        <TableCell>{item.orderType}</TableCell>
                                        <TableCell>{item.price}</TableCell>
                                    </TableRow>
                                )}
                            </TableBody>
                        </Table>
                    </TableContainer>
                    <br />
                </Paper>
                </Grid>
            </Grid>
        </div>
    )
}
