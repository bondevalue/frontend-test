import { useEffect, useState } from 'react'
import { Table, TableCell, TableBody, TableRow, TableContainer, TableHead, Paper, Button } from '@mui/material';

export function TableElement() {
    const [tableData, setTableData] = useState([]);

    useEffect(() => {
        fetch("http://localhost:3000/orders/")
            .then(response => response.json())
            .then(data => setTableData(data.data))
            .catch(err => console.error(err))
    }, [])

    const handleMatch = () => {

    };

    return (
        <div>
            <Paper>
                <h3>Placed Orders</h3>
                <TableContainer
                    component={Paper}
                    variant="outlined"
                >
                    <Table aria-label="demo table">
                        <TableHead>
                            <TableRow>
                                <TableCell>Name</TableCell>
                                <TableCell>Order Type</TableCell>
                                <TableCell>Price</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {tableData && tableData.map((item) =>
                                <TableRow key={item.id}>
                                    <TableCell>{item.name}</TableCell>
                                    <TableCell>{item.orderType}</TableCell>
                                    <TableCell>{item.price}</TableCell>
                                </TableRow>
                            )}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Paper>
            <br/>
        </div>
    )
}