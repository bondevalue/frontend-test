FROM node:14 AS build
WORKDIR /app
COPY . .
RUN npm install
RUN npm run build

FROM nginx:alpine
EXPOSE 80
COPY --from=build /app/dist /usr/share/nginx/html